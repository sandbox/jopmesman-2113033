README.txt
==========

This module catches mail and forward them to an pre-configured email-address
At the admin-page the user can configure which modules should forward the mail
to which emailaddress. Also could the user set an prefix which will be added to
the subject.

If the user set the prefix to 123, the subject wil be prefixed with
"(123) subject" This can be handy when there is a development street. You can
add for example the environment.


Test the module
===============

- Go to the admin page of the module (admin/settings/mailredirect7)
- Add, if not already done, the mailredirect7 module in the modules fieldlist.
- Save the admin form
- Go to the testpage (mailredirect7/testform)
- Fill out the form and hit the send button.


Installation
============

Copy the folder mailredirect7 to your module folder and then enable it on the
admin module page. To configure the module go to 'admin/settings/mailredirect7'.


Settings
========

- To: who will the redirected mail(s) be send
This is a list of emailaddresses where the mails will be redirected to.
In this textfield you can add more addresses separated by comma's
- Modules:
This is the list of modules which implements hook_mail.
You can select which modules should redirect their emails. You can choose to
unselect modules if it's not necessary for some reason.
- Subjectprefix:
If you want, you can let the mailredirect prefix the subject by the
subjectprefix. For example, You filled in '123'. The subject will be
'(123) {subject}' This comes in handy when you have different environments.
Like a development, test or acceptance environment.


Under the hood
==============

This module implements the hook 'hook_mail_alter'
Everytime a mail is send via drupal_mail, the mail will fire up
the hook_mail_alter of this module.

At first the module checks if the mail is from a module which is selected in the
list of modules. If it does, the 'to' address will be replaced by the
preconfigured emailaddress in the administrationform. If the preconfigured email
is empty the site emailaddress will be used.

Also will a header text added to the body.

At last the module checks if a subjectprefix is needed. If it's needed it will
add the prefix to the subject.

Future plans
============

- Add also a text to the bottom of the body/message.
- Make the header and footer text editable in the admin page.


Author
======

Jop Mesman
