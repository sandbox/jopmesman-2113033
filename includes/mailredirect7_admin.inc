<?php

/**
 * @file
 * This file is for the admin part of the mailredirect7 module
 */

/**
 * Implements hook_form().
 */
function mailredirect7_admin_form($form, &$form_state) {
  $form = array();

  // Get the modules which implementing hook_mail.
  $modules = mailredirect7_getmodules();

  // Which modules should redirect.
  // In the viable_get are the modules saved.
  // These are selected by the administrator.
  $redirect = variable_get('mailredirect7_modules', array());

  // Determine if the mailredirect7 modules is selected in the list.
  $linktotestpage = l(t('Test mailredirect7'), 'mailredirect7/testform');
  $textabouttextpage = t('Select mailredirect7 module to test this module. If you add it and save, at this place appears a link to the testpage.');
  if (isset($redirect['mailredirect7']) and $redirect['mailredirect7'] === 'mailredirect7') {
    $markup = $linktotestpage;
  }
  else {
    $markup = $textabouttextpage;
  }

  // Link to the testpage. If the mailredirect7 module is not selected,
  // Give the message.
  $form['linktotestpage'] = array(
    '#markup' => $markup,
  );

  // List of emailaddresses.
  $form['mailredirect7_to_list'] = array(
    '#title' => t('To who will the redirected mail(s) be send'),
    '#description' => t('Give a list of emailaddresses. This emailaddresses will be used to redirect the mail to. To give more emailaddresses use a comma to separate.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('mailredirect7_to_list', ''),
  );

  // Name of the fieldset.
  $fieldset_id = 'mailredirect7_admin_fieldset';
  // Define the fieldset.
  $form[$fieldset_id] = array(
    '#type' => 'fieldset',
    '#title' => t('Modules'),
  );

  // List of modules with checkboxes.
  // Select which modules should redirect it's mails.
  $form[$fieldset_id]['mailredirect7_modules'] = array(
    '#type' => 'checkboxes',
    '#options' => $modules,
    '#default_value' => $redirect,
  );

  $form['mailredirect7_subjectprefix'] = array(
    '#type' => 'radios',
    '#title' => t('Subjectprefix'),
    '#description' => t('If this is filled, this will be prefixed at the beginning of the subject. E.G. (123) Welcome new user.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('mailredirect7_subjectprefix', ''),
  );

  // Return it as a system_settings_form
  return system_settings_form($form);
}
